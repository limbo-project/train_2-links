## REQUIRED parameters ###
SOURCE_ID=org.limbo:train_1-dataset:1.0.0
# The version of the Limbo dataset
TARGET_ID=wikidata
VERSION=1.0.2-SNAPSHOT
# The Limbo dataset license
LIMBO_DATASET_LICENSE=NullLicense

